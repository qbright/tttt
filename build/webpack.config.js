const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var UglifyJsPlugin = require("uglifyjs-webpack-plugin");
var OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const merge = require("webpack-merge");
const { NODE_ENV } = process.env;

const flexable = require.resolve("amfe-flexible/index.min.js");
const fs = require("fs");

const amfeFlexible = fs.readFileSync(flexable, { encoding: "utf8" });
const baseConfg = {
  entry: "./src/index.ts",
  mode: NODE_ENV,
  devtool: NODE_ENV === "production" ? false : "eval-source-map",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: /\.scss$|\.css$/,
        use:
          NODE_ENV === "production"
            ? [
                // "style-loader", // creates style nodes from JS strings
                MiniCssExtractPlugin.loader,
                "css-loader",
                "postcss-loader",
                "sass-loader"
              ]
            : ["style-loader", "css-loader", "sass-loader", "postcss-loader"]
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              publicPath: "/",
              name: "./img/[hash:7].[ext]",
              limit: 10
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  output: {
    filename: "./js/main.[chunkhash:7].js",
    path: path.resolve(__dirname, "../dist/")
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: "./src/index.ejs",
      amfeFlexible
      //https://github.com/jantimon/html-webpack-plugin#options
    })
  ]
};

let buildConfig = {};

if (NODE_ENV === "development") {
  buildConfig = merge.smart(baseConfg, {
    devServer: {
      stats: {
        children: false,
        chunkOrigins: false,
        modules: false
      },
      compress: true
    }
  });
} else {
  buildConfig = merge.smart(baseConfg, {
    plugins: [
      new MiniCssExtractPlugin({ filename: "./style/index.[hash:7].css" })
    ],
    optimization: {
      minimizer: [
        // 自定义js优化配置，将会覆盖默认配置
        new UglifyJsPlugin({
          exclude: /\.min\.js$/, // 过滤掉以".min.js"结尾的文件，我们认为这个后缀本身就是已经压缩好的代码，没必要进行二次压缩
          cache: true,
          parallel: true, // 开启并行压缩，充分利用cpu
          sourceMap: false,
          extractComments: false, // 移除注释
          uglifyOptions: {
            compress: {
              unused: true,
              drop_debugger: true
            },
            output: {
              comments: false
            }
          }
        }),
        // 用于优化css文件
        new OptimizeCssAssetsPlugin({
          assetNameRegExp: /\.css$/g,
          cssProcessorOptions: {
            safe: true,
            autoprefixer: { disable: true }, // 这里是个大坑，稍后会提到
            mergeLonghand: false,
            discardComments: {
              removeAll: true // 移除注释
            }
          },
          canPrint: true
        })
      ]
    }
  });
}

module.exports = buildConfig;
