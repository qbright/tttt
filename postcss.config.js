const px2rem = require("postcss-px2rem");

module.exports = {
  plugins: [
    require("autoprefixer")({
      overrideBrowserslist: ["> 1%", "last 2 versions", "not ie <= 8"]
    }),
    require("postcss-plugin-px2rem")({
      rootValue: 75,
      exclude: /(node_module)/
    })
  ]
};
